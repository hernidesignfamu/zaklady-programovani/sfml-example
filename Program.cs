﻿using System;
using System.Collections.Generic;

using SFML.Graphics;
using SFML.Window;

namespace ZakladyProgramovaniExampleGame
{
    // This class just holds some constants that are used through the game
    class Constants
    {
        // Size of one tile / sprite
        public static int TILE_SIZE = 16;
        // Map width and height (in number of tiles)
        public static int MAP_WIDTH = 10;
        public static int MAP_HEIGHT = 10;
        // Screen width and height (in pixels)
        public static int SCREEN_WIDTH = TILE_SIZE * MAP_WIDTH;
        public static int SCREEN_HEIGHT = TILE_SIZE * MAP_HEIGHT;
    }

    // This class provides basic random generation functions
    class RandomManager
    {
        static Random randomGenerator = new Random();

        // Generates a number from 1 to X
        public static int DiceRoll (int sides)
        {
            return randomGenerator.Next(1, sides+1);
        }

        // Generates a number from 0 to X
        public static int ZeroRoll (int max)
        {
            return randomGenerator.Next(0, max);
        }
    }

    // This class handles loading textures from the spritesheet and providing them to other parts of the game
    class TextureManager
    {
        // Path to the spritesheet
        static string TILEMAP_PATH = "assets/colored_packed.png";

        // Function that handles loading a single texture by "cutting" it out from the spritesheet based on its location
        static Texture LoadTextureFromTileMap (int posX, int posY) 
        {
            IntRect area = new IntRect(posX * Constants.TILE_SIZE, posY * Constants.TILE_SIZE, Constants.TILE_SIZE, Constants.TILE_SIZE);
            return new Texture(TILEMAP_PATH, area);
        }

        //
        // All textures are loaded automatically at the beginning
        //

        // Player
        public static Texture PLAYER = LoadTextureFromTileMap(25, 0);

        // Basic ground texture
        public static Texture GROUND = LoadTextureFromTileMap(0, 0);
        // Treeeeees
        public static Texture TREE = LoadTextureFromTileMap(0, 1);
        
        // Houses
        public static Texture HOUSE_RED = LoadTextureFromTileMap(0, 19);
    }

    // This class represents on tile on the map grid
    class Tile
    {
        // This holds the Sprite that is drawn on rendering
        Sprite sprite;

        // This holds tiles position on the grid (in number of tiles)
        public int x;
        public int y;

        // This determines whether the Tile already has a point of interest (house, tree, cave, etc.)
        public bool hasPoi {get; set;}

        // Constructor takes position and Texture to be used to render the Tile
        public Tile (int posX, int posY, Texture texture)
        {
            hasPoi = false;
            x = posX;
            y = posY;
            sprite = new Sprite(texture);
            sprite.Position = new SFML.System.Vector2f(x * Constants.TILE_SIZE, y * Constants.TILE_SIZE);
        }

        // This function allows us to replace the texture used for Tile rendering
        public void ReplaceTexture (Texture texture)
        {
            sprite.Texture = texture;
        }

        // This function renders this Tile using the passed RenderWindows instance
        public void Render (RenderWindow rw)
        {
            rw.Draw(sprite);
        }
    }

    // This class represents the whole map, whis is basically a List of Tiles
    class Map
    {
        public List<Tile> tiles = new List<Tile>();

        // Constructor generates the whole map, filling it with Tiles that have the GROUND texture
        public Map ()
        {
            // On map creation, we fill it with GROUND Tiles
            int x = 0;
            int y = 0;

            while (y <= Constants.MAP_HEIGHT) 
            {
                x = 0;
                while (x <= Constants.MAP_WIDTH)
                {
                    tiles.Add(new Tile(x, y, TextureManager.GROUND));
                    // This is the same as x = x + 1;
                    x++;
                }
                // This is the same as y = y + 1;
                y++;
            }

            // After map is generated, we place some points of interest on it
            GeneratePointsOfInterest();
        }

        // This function returns a Tile at specific coordinates, or null if the tile does not exist (that's what the ? in the type definition does)
        public Tile? GetTileAtCoords (int x, int y)
        {
            foreach (Tile t in tiles)
            {
                if (t.x == x && t.y == y)
                {
                    return t;
                }
            }

            return null;
        }

        // This function generates trees, houses, etc. and places them somwhere on the map where no other POIs are already located
        private void GeneratePointsOfInterest ()
        {
            // Trees
            int numberOfTrees = 0;
            while (numberOfTrees < 5)
            {
                int treeX = RandomManager.ZeroRoll(Constants.MAP_WIDTH);
                int treeY = RandomManager.ZeroRoll(Constants.MAP_HEIGHT);

                Tile? t = GetTileAtCoords(treeX, treeY);

                if (t != null && !t.hasPoi)
                {
                    Console.WriteLine("Tree - Coords: " + treeX + ", " + treeY + ", " + t.hasPoi);
                    t.ReplaceTexture(TextureManager.TREE);
                    t.hasPoi = true;
                    numberOfTrees++;
                }
            }

            // Houses
            int numberOfHouses = 0;
            while (numberOfHouses < 3)
            {
                int houseX = RandomManager.ZeroRoll(Constants.MAP_WIDTH);
                int houseY = RandomManager.ZeroRoll(Constants.MAP_HEIGHT);

                Tile? t = GetTileAtCoords(houseX, houseY);

                if (t != null && !t.hasPoi)
                {
                    Console.WriteLine("House - Coords: " + houseX + ", " + houseY + ", " + t.hasPoi);
                    t.ReplaceTexture(TextureManager.HOUSE_RED);
                    t.hasPoi = true;
                    numberOfHouses++;
                }
            }

            // Caves
            // === TBD

            // Castle
            // === TBD
        }
    }

    // This class represents an Item and currently has no further implementation
    class Item
    {
        public string name;

        public bool givesHealth = false;
        public int damage = 0;
        public int strength = 0;
        public int intelligence = 0;
        public int dexterity = 0;

        public Item (string n)
        {
            name = n;
        }
    }

    // This class represents the Player
    class Player
    {
        public string name;
        public List<Item> inventory;

        private int strength = 5;
        private int intelligence = 5;
        private int dexterity = 5;

        public int posX = 0;
        public int posY = 0;

        private bool isInEncounter = false;

        Sprite sprite;

        public Player (string n)
        {
            name = n;

            inventory = new List<Item>();
            sprite = new Sprite(TextureManager.PLAYER);
        }

        public void describeAttributes ()
        {
            int str = strength;
            int intl = strength;
            int dex = strength;

            foreach (Item i in inventory)
            {
                str = str + i.strength;
                intl = intl + i.intelligence;
                dex = dex + i.dexterity;
            }

            Console.WriteLine("Hrac: " + name);
            Console.WriteLine("STR: " + str);
            Console.WriteLine("INT: " + intl);
            Console.WriteLine("DEX: " + dex);
        }

        public void moveLeft ()
        {
            int newPosX = posX - 1;

            if (newPosX >= 0)
            {
                posX = newPosX;
            }
        }

        public void moveRight ()
        {
            int newPosX = posX + 1;

            if (newPosX >= 0 && newPosX < Constants.MAP_WIDTH)
            {
                posX = newPosX;
            }
        }

        public void moveUp ()
        {
            int newPosY = posY - 1;

            if (newPosY >= 0)
            {
                posY = newPosY;
            }
        }

        public void moveDown ()
        {
            int newPosY = posY + 1;

            if (newPosY >= 0 && newPosY < Constants.MAP_HEIGHT)
            {
                posY = newPosY;
            }
        }

        // This function is called on each KeyPress and updates player's position accordingly
        public void Update ()
        {
            if (Keyboard.IsKeyPressed(Keyboard.Key.W))
            {
                moveUp();
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.S))
            {
                moveDown();
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.A))
            {
                moveLeft();
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.D))
            {
                moveRight();
            }
        }

        // This function renders the Player using the passed RenderWindow
        public void Render (RenderWindow rw)
        {
            // We always assign the player's sprite a new position, because he moves and we want to render him at his current coordinates
            sprite.Position = new SFML.System.Vector2f(posX * Constants.TILE_SIZE, posY * Constants.TILE_SIZE);
            rw.Draw(sprite);
        }
    }

    // This is the main class of the program, that handles initialization and basic game loop
    class Program
    {
        static RenderWindow? renderWindow;
        static Map map = new Map();
        static Player player = new Player("Andrej");

        static void Main(string[] args)
        {
            Console.WriteLine("========");
            Console.WriteLine("THE MOST GENERIC DUNGEON GAME EVER CREATED");
            Console.WriteLine("(c) 2022 ANDREJ SYKORA");
            Console.WriteLine("========");

            Init();
        }

        static void Init ()
        {
            renderWindow = new RenderWindow(new VideoMode((uint) Constants.SCREEN_WIDTH, (uint) Constants.SCREEN_HEIGHT), "GENERIC DUNGEON");
            renderWindow.KeyPressed += Window_KeyPressed;

            while (renderWindow.IsOpen)
            {
                GameLoop();
            }
        }

        static void Window_KeyPressed (object sender, KeyEventArgs e)
        {
            // Update the player position according to the key pressed
            player.Update();
            // Check what is on the tile he is at and display description in the console
        }

        static void GameLoop ()
        {
            if (renderWindow != null)
            {
                // At the beginning of every frame, we dispatch events (keyboard presses, etc.)             
                renderWindow.DispatchEvents();

                // We clear the rendering surface with black color
                renderWindow.Clear(Color.Black);

                // We render all the tiles in the map
                foreach (Tile t in map.tiles)
                {
                    t.Render(renderWindow);
                }

                // We render the player
                player.Render(renderWindow);

                // We display everything
                renderWindow.Display();
                
            } else {
                throw new Exception("Somehow, renderWindow became null?");
            }            
        }
    }
}
